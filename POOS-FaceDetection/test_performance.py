from imutils import paths
import argparse
import cv2
import os
from detect_faces import faces
from fun_prepoznaj import prepoznaj_lice
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--dataset", required=True,)
ap.add_argument("-r", "--SVM", required=True)
ap.add_argument("-l", "--name", required=True)
args = vars(ap.parse_args())

print("ucitavnaje slika")
imagePaths = list(paths.list_images(args["dataset"]))

acc_br=0
sp_br=0
se_br=0

for (i, imagePath) in enumerate(imagePaths):
    image = cv2.imread(imagePath)
    print("obrada slike: ",i+1,"/",len(imagePaths))
    ime = imagePath.split(os.path.sep)[-2]

    face=faces(image)
    if len(face)>0:
        pime=prepoznaj_lice(face[0],args)

        if(pime==ime):
            acc_br+=1

        if(ime!="nepoznato" and pime!="nepoznato"):
            se_br+=1

        if(ime=="nepoznato" and pime=="nepoznato"):
            sp_br+=1

print("acc_br = ",acc_br)
print("se_br = ",se_br)
print("sp_br = ",sp_br)
print("{} {:.2f}%".format("acc = ",acc_br/len(imagePaths)*100))
print("{} {:.2f}%".format("se = ",se_br/27*100))
print("{} {:.2f}%".format("sp = ",sp_br/6*100))
