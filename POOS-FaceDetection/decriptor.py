
import cv2


def create_descriptor(face):
    embedder = cv2.dnn.readNetFromTorch("openface_nn4.small2.v1.t7")
    faceBlob = cv2.dnn.blobFromImage(face, 1.0 / 255,(96, 96), (0, 0, 0), swapRB=True, crop=False)
    embedder.setInput(faceBlob)
    a=embedder.forward()
    return a
