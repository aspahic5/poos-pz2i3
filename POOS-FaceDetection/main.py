
import numpy as np
import argparse
import pickle
import cv2
from detect_faces import faces
from decriptor import create_descriptor
from imutils import paths


ap = argparse.ArgumentParser()

ap.add_argument("-r", "--recognizer", required=True)
ap.add_argument("-l", "--name", required=True)

args = vars(ap.parse_args())


def prepoznaj(url,args):
    recognizer = pickle.loads(open(args["recognizer"], "rb").read())
    name = pickle.loads(open(args["name"], "rb").read())
    image = cv2.imread(url)

    detect=cv2.CascadeClassifier('./haarcascade_frontalface_default.xml')#klasifikator za lice
    detect_eye=cv2.CascadeClassifier('./haarcascade_eye.xml')#klasifikator za oci

    #detektovanje lica
    faces = detect.detectMultiScale(image,scaleFactor=1.2)
    #obiljezi lica
    for (x, y, w, h) in faces:
        #face_region uzima samo dio slike koji sadrzi lice
        face= image[y:y+h, x:x+w]
        #detektuj oci na licu
        eyes=detect_eye.detectMultiScale(face)
        #ako sadrzi oci oznaci lice
        if len(eyes)>0:

            deskriptor=create_descriptor(face)
            preds = recognizer.predict_proba(deskriptor)[0]
            j = np.argmax(preds)
            vjerovatnoca = preds[j]
            ime = name.classes_[j]
            text = "{}: {:.2f}%".format(ime, vjerovatnoca * 100)
            y = y - 10 if y - 10 > 10 else y + 10
            cv2.rectangle(image, (x, y), (x+w, y+h),(0, 255, 0), 2)
            cv2.putText(image, text, (x, y),cv2.FONT_HERSHEY_SIMPLEX, 0.5,(0, 0, 255), 2)


    cv2.imshow("Image", image)
    cv2.waitKey(0)


def main(path,args):

    imagePaths = list(paths.list_images(path))

    for (i,imageP) in enumerate(imagePaths):
        prepoznaj(imageP,args)

main("/home/amar/PycharmProjects/POOS-FaceDetection/Validacija/",args)
