import cv2


def faces(img):
    detect=cv2.CascadeClassifier('./haarcascade_frontalface_default.xml')#klasifikator za lice
    detect_eye=cv2.CascadeClassifier('./haarcascade_eye.xml')#klasifikator za oci
    image= img

    detections=[]
    #detektovanje lica
    faces = detect.detectMultiScale(image,scaleFactor=1.2)
    #obiljezi lica
    for (x, y, w, h) in faces:
        #face_region uzima samo dio slike koji sadrzi lice
        face_region = image[y:y+h, x:x+w]
        #detektuj oci na licu
        eyes=detect_eye.detectMultiScale(face_region)
        #ako sadrzi oci oznaci lice
        if len(eyes)>0:
            detections.append(face_region)

    return detections


#test detektovanja lica
'''
lica=faces("testimages/17.jpg")
for i in range (0,len(lica)):
    name="lice"+str(i+1)
    cv2.imshow(name,lica[i])

cv2.waitKey(0)
cv2.destroyAllWindows()
'''


