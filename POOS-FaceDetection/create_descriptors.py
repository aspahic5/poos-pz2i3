
from imutils import paths
import argparse
import pickle
import cv2
import os
from detect_faces import faces
from  decriptor import create_descriptor

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--dataset", required=True,)
ap.add_argument("-e", "--deskriptori", required=True)

args = vars(ap.parse_args())

print("ucitavnaje slika")
imagePaths = list(paths.list_images(args["dataset"]))


deskriptori = []
imena = []


for (i, imagePath) in enumerate(imagePaths):
	print("obrada slike: ",i+1,"/",len(imagePaths))
	ime = imagePath.split(os.path.sep)[-2]

	image = cv2.imread(imagePath)

	imageBlob = cv2.dnn.blobFromImage(
		cv2.resize(image, (300, 300)), 1.0, (300, 300),
		(104.0, 177.0, 123.0), swapRB=False, crop=False)


	detections = faces(image)


	if len(detections) > 0:

		imena.append(ime)
		deskriptori.append(create_descriptor(detections[0]).flatten())



print("spremanje deskriptora u datoteku deskriptori.pickle")
data = {"deskriptori": deskriptori, "names": imena}
print(data)
f = open(args["deskriptori"], "wb")
f.write(pickle.dumps(data))
f.close()
print("deskriptori spremljeni")
