
from sklearn.preprocessing import LabelEncoder
from sklearn.svm import SVC
import argparse
import pickle


ap = argparse.ArgumentParser()
ap.add_argument("-e", "--deskriptori", required=True)
ap.add_argument("-r", "--SVM", required=True)
ap.add_argument("-l", "--imena", required=True)
args = vars(ap.parse_args())

data = pickle.loads(open(args["deskriptori"], "rb").read())
le = LabelEncoder()
labels = le.fit_transform(data["names"])


print("treniranje pocetak")
SVM = SVC(C=5.0, kernel="linear", probability=True,max_iter=-1)
SVM.fit(data["deskriptori"], labels)
print("treniranje zavrseno")

print("spremanje")
f = open(args["SVM"], "wb")
f.write(pickle.dumps(SVM))
f.close()


f = open(args["imena"], "wb")
f.write(pickle.dumps(le))
f.close()
