import cv2

def face_detect(url):
    detect=cv2.CascadeClassifier('./haarcascade_frontalface_default.xml')#klasifikator za lice
    detect_eye=cv2.CascadeClassifier('./haarcascade_eye.xml')#klasifikator za oci
    image= cv2.imread(url)#url slike

    #detektovanje lica
    faces = detect.detectMultiScale(image)
    detections=[]
    #obiljezi lica
    for (x, y, w, h) in faces:
        #face_region uzima samo dio slike koji sadrzi lice
        face_region = image[y:y+h, x:x+w]
        #detektuj oci na licu
        eyes=detect_eye.detectMultiScale(face_region)
        #ako sadrzi oci oznaci lice
        if len(eyes)>0:
            detections.append(face_region)

    return detections

#test
lica=face_detect("testimages/17.jpg")
for x in lica:
    cv2.imshow('lice',x)

cv2.waitKey(0)
cv2.destroyAllWindows()



