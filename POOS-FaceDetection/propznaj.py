
import numpy as np
import argparse
import pickle
import cv2
from detect_faces import faces
from decriptor import create_descriptor

#url slike
img="/home/amar/PycharmProjects/POOS-FaceDetection/testimages/CristianoRonaldo/6.jpg"

ap = argparse.ArgumentParser()

ap.add_argument("-r", "--recognizer", required=True,
	help="path to model trained to recognize faces")
ap.add_argument("-l", "--name", required=True,
	help="path to label encoder")

args = vars(ap.parse_args())



# load the actual face recognition model along with the label encoder
recognizer = pickle.loads(open(args["recognizer"], "rb").read())
name = pickle.loads(open(args["name"], "rb").read())


image = cv2.imread(img)

detect=cv2.CascadeClassifier('./haarcascade_frontalface_default.xml')#klasifikator za lice
detect_eye=cv2.CascadeClassifier('./haarcascade_eye.xml')#klasifikator za oci

#detektovanje lica
faces = detect.detectMultiScale(image,scaleFactor=1.2)
#obiljezi lica
for (x, y, w, h) in faces:
    #face_region uzima samo dio slike koji sadrzi lice
    face= image[y:y+h, x:x+w]
    #detektuj oci na licu
    eyes=detect_eye.detectMultiScale(face)
    #ako sadrzi oci oznaci lice
    if len(eyes)>0:

        deskriptor=create_descriptor(face)
        preds = recognizer.predict_proba(deskriptor)[0]
        j = np.argmax(preds)
        vjerovatnoca = preds[j]
        ime = name.classes_[j]
        text = "{}: {:.2f}%".format(ime, vjerovatnoca * 100)
        y = y - 10 if y - 10 > 10 else y + 10
        cv2.rectangle(image, (x, y), (x+w, y+h),(0, 255, 0), 2)
        cv2.putText(image, text, (x, y+h),cv2.FONT_HERSHEY_SIMPLEX, 0.5,(0, 0, 255), 2)


cv2.imshow("Image", image)
cv2.waitKey(0)
