
import numpy as np
import argparse
import pickle
import cv2
from decriptor import create_descriptor


def prepoznaj_lice(img_face, args):




    # load the actual face recognition model along with the label encoder
    recognizer = pickle.loads(open(args["SVM"], "rb").read())
    name = pickle.loads(open(args["name"], "rb").read())

    deskriptor=create_descriptor(img_face)
    preds = recognizer.predict_proba(deskriptor)[0]
    j = np.argmax(preds)
    vjerovatnoca = preds[j]
    ime = name.classes_[j]
    return ime
